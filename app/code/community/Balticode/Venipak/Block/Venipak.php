<?php

class Balticode_Venipak_Block_Venipak extends Mage_Checkout_Block_Onepage_Shipping_Method_Available
{

    public $_availableFields;
    public $_deliveryTimes;
    public $_quoteId;

    public function __construct()
    {
        $this->setTemplate('venipak/venipak.phtml');
        $store_id = Mage::app()->getStore()->getId(); //Get Current store id
        $this->_collectAvailableFields($store_id); //Collect all Availible Filds time,warehouse, office nr, door code
        $this->_collectDeliveryTimes($store_id); //Collect all Availible times: nwd,nwd10,nwd12,nwd8_14,nwd14_17,nwd18_20,nwd18a,sat
        $this->_quoteId = $this->getQuote()->getId();
    }

     /**
     * Get Quote data from balticode venipak Quote data table
     *
     * @param String $column
     * @return Mix
     */
    public function getQuoteValue($column = '')
    {
        $data = Mage::getModel('venipak/orderdata')->load($this->_quoteId)->getData($column); //Get info by quote
        if (!isset($data)) {
            $data = Mage::helper('venipak/data')->getConfigData($column); //if not found info by orders grab default value
        }

        return $data;
    }

    public function _collectAvailableFields($store = null)
    {
        $available_fields = Mage::helper('venipak/data')->getConfigData('extra_filed', $store);
        $this->_availableFields = unserialize($available_fields);

        return $this;
    }

    public function _collectDeliveryTimes($store = null)
    {
        $deliveryTimes = Mage::helper('venipak/data')->getConfigData('delivery_time', $store);
        $this->_deliveryTimes = array_filter(explode(',', $deliveryTimes));

        return $this;
    }

    public function getAvailableFields()
    {
        return $this->_availableFields;
    }

    public function getDeliveryTimes()
    {
        return $this->_deliveryTimes;
    }

    public function isAvailableFiled($name)
    {
        return $this->_availableFields[$name];
    }

    public function isDeliveryTime($time)
    {
        return in_array($time, $this->_deliveryTimes);
    }

    public function getAvailableFiledLabel($field_code)
    {
        $AllAvailibleFields = Mage::getModel('venipak/system_config_source_availablefields')->toArray();
        if (!isset($AllAvailibleFields[$field_code])) {
            return false;
        }

        return $AllAvailibleFields[$field_code];
    }

    public function getDeliveryTimeLabel($time_code)
    {
        return Mage::helper('venipak/data')->getDeliveryTimeLabel($time_code);
    }

    public function getWarehouseHtml($entity_id = null, $name)
    {
        $data = $this->getQuoteValue('warehouse_id');
        if (empty($data)) {
            $data = Mage::helper('venipak/data')->default_warehouse_id;
        }
        $helper = Mage::helper('venipak/data');
        $list = $helper->getWarehouseList();
        $list = $helper->flipWarehouseList($list);
        $html = '<select name="'.$name.'">';
        foreach ($list as $key => $value) {
            $html .= '<option value="'.$key.'" '
                .(($key==$data)?' selected="selected" ':'')
                .' >'.$value['c_name'].' / '.$value['address'].', '.$value['c_city'].'</option>';
        }
        $html .= '</select>';

        return $html;
    }
}
