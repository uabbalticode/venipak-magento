<?php

class Balticode_Venipak_Block_Adminhtml_Warehouse extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected $_addRowButtonHtml = array();
    protected $_removeRowButtonHtml = array();

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $html = '<div class="grid" >';
        $html .= '<table style="display:none">';
        $html .= '<tbody id="venipak_warehouse_template">';
        $html .= $this->_getRowTemplateHtml();
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<table class="border" cellspacing="0" cellpadding="0">';
        $html .= '<tbody id="venipak_warehouse_container">';
        $html .= '<tr class="headings">';
        $html .= '<th>'.$this->__('Name').'</th>';
        $html .= '<th>'.$this->__('Company code').'</th>';
        $html .= '<th>'.$this->__('Country ISO').'</th>';
        $html .= '<th>'.$this->__('City').'</th>';
        $html .= '<th>'.$this->__('Address').'</th>';
        $html .= '<th>'.$this->__('Post code').'</th>';
        $html .= '<th>'.$this->__('Contact Name').'</th>';
        $html .= '<th>'.$this->__('Contact Tel. Nr.').'</th>';
        $html .= '<th>&nbsp;</th>';
        $html .= '</tr>';
        if ($this->_getValue('c_name')) {
            foreach ($this->_getValue('c_name') as $i=>$f) {
                if ($i) {
                    $html .= $this->_getRowTemplateHtml($i);
                }
            }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= $this->_getAddRowButtonHtml(
            'venipak_warehouse_container',
            'venipak_warehouse_template',
            $this->__('Add combination')
        );

        return $html;
    }

    protected function _getRowTemplateHtml($i=0)
    {
        $html = '<tr>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[c_name][]" value="'.$this->_getValue('c_name/'.$i).'" style="width:60px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[c_code][]" value="'.$this->_getValue('c_code/'.$i).'" style="width:80px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[iso][]" value="'.$this->_getValue('iso/'.$i).'" style="width:60px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[c_city][]" value="'.$this->_getValue('c_city/'.$i).'" style="width:80px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[address][]" value="'.$this->_getValue('address/'.$i).'" style="width:100px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[p_code][]" value="'.$this->_getValue('p_code/'.$i).'" style="width:50px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[name][]" value="'.$this->_getValue('name/'.$i).'" style="width:80px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[tel][]" value="'.$this->_getValue('tel/'.$i).'" style="width:80px;" />';
        $html .= '<input type="hidden" name="'.$this->getElement()->getName().'[id][]" value="'.$i.'"/>';
        $html .= '</td>';
        $html .= '<td>'.$this->_getRemoveRowButtonHtml().'</td>';
        $html .= '</tr>';

        return $html;
    }

    protected function _getDisabled()
    {
        return $this->getElement()->getDisabled() ? ' disabled' : '';
    }

    protected function _getValue($key)
    {
        return $this->getElement()->getData('value/'.$key);
    }

    protected function _getAddRowButtonHtml($container, $template, $title = 'Add')
    {
        if (!isset($this->_addRowButtonHtml[$container])) {
            $this->_addRowButtonHtml[$container] = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('add '.$this->_getDisabled())
            ->setLabel($this->__($title))
            ->setOnClick("Element.insert($('".$container."'), {bottom: $('".$template."').innerHTML})")
            ->setDisabled($this->_getDisabled())
            ->toHtml();
        }

        return $this->_addRowButtonHtml[$container];
    }

    protected function _getRemoveRowButtonHtml($selector='tr', $title='Delete')
    {
        if (!$this->_removeRowButtonHtml) {
            $this->_removeRowButtonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('delete v-middle '.$this->_getDisabled())
            ->setLabel($this->__($title))
            ->setOnClick("Element.remove($(this).up('".$selector."'))")
            ->setDisabled($this->_getDisabled())
            ->toHtml();
        }

        return $this->_removeRowButtonHtml;
    }
}
