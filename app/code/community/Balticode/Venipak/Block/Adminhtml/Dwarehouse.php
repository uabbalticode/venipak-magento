<?php

class Balticode_Venipak_Block_Adminhtml_Dwarehouse extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $html = '<div class="grid" >';
        $html .= '<table class="border" cellspacing="0" cellpadding="0">';
        $html .= '<tbody id="venipak_default_warehouse_container">';
        $html .= '<tr class="headings">';
        $html .= '<th>'.$this->__('Name').'</th>';
        $html .= '<th>'.$this->__('Company code').'</th>';
        $html .= '<th>'.$this->__('Country ISO').'</th>';
        $html .= '<th>'.$this->__('City').'</th>';
        $html .= '<th>'.$this->__('Address').'</th>';
        $html .= '<th>'.$this->__('Post code').'</th>';
        $html .= '<th>'.$this->__('Contact Name').'</th>';
        $html .= '<th>'.$this->__('Contact Tel. Nr.').'</th>';
        $html .= '</tr>';
        $html .= '</tbody>';
        $html .= '<tr>';
        $html .= '<td><input class="required-entry input-text" type="text" name="'.$this->getElement()->getName().'[c_name][]" value="'.$this->_getValue('c_name/0').'" style="width:60px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[c_code][]" value="'.$this->_getValue('c_code/0').'" style="width:80px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[iso][]" value="'.$this->_getValue('iso/0').'" style="width:60px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[c_city][]" value="'.$this->_getValue('c_city/0').'" style="width:80px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[address][]" value="'.$this->_getValue('address/0').'" style="width:100px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[p_code][]" value="'.$this->_getValue('p_code/0').'" style="width:50px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[name][]" value="'.$this->_getValue('name/0').'" style="width:80px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[tel][]" value="'.$this->_getValue('tel/0').'" style="width:80px;" />';
        $html .= '<input type="hidden" name="'.$this->getElement()->getName().'[id][]" value="1"/></td>';
        $html .= '</tr>';
        $html .= '</table>';
        $html .= '</div>';

        return $html;
    }

    protected function _getValue($key)
    {
        return $this->getElement()->getData('value/'.$key);
    }

}
