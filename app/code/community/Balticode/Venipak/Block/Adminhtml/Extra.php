<?php

class Balticode_Venipak_Block_Adminhtml_Extra extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $html = '<div class="grid" >';
        $html .= '<table style="display:none">';
        $html .= '<tbody id="venipak_extra_template">';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<table class="border" cellspacing="0" cellpadding="0">';
        $html .= '<tbody id="venipak_container">';
        $html .= '<tr class="headings">';
        $html .= '<th>'.$this->__('Shipping time').'</th><th>'.$this->__('Door code').'</th><th>'.$this->__('Office nr.').'</th><th>'.$this->__('Warehouse').'</th><th>'.$this->__('Call before delivery').'</th>';
        $html .= '</tr>';
        $html .= '<tr><td>'.$this->_getSelectOptionsNoYes('time').'</td>';
        $html .= '<td>'.$this->_getSelectOptionsNoYes('door').'</td>';
        $html .= '<td>'.$this->_getSelectOptionsNoYes('office').'</td>';
        $html .= '<td>'.$this->_getSelectOptionsNoYes('warehouse').'</td>';
        $html .= '<td>'.$this->_getSelectOptionsNoYes('call').'</td></tr>';
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';

        return $html;
    }

    protected function _getSelectOptionsNoYes($name)
    {
        $html = '<select id="venipak_'.$name.'" class="option-control" style="width: 80px" value="" name="'.$this->getElement()->getName().'['.$name.']" >';
        $html .= '<option value="0" '.(($this->_getValue($name)==0)?"selected":"").'>'.__('No').'</option>';
        $html .= '<option value="1" '.(($this->_getValue($name)==1)?"selected":"").'>'.__('Yes').'</option>';
        $html .= '</select>';

        return $html;
    }

    protected function _getValue($key)
    {
        return $this->getElement()->getData('value/'.$key);
    }

}
