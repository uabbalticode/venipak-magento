<?php

class Balticode_Venipak_Block_Adminhtml_Deliveryprices extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected $_addRowButtonHtml = array();
    protected $_removeRowButtonHtml = array();

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $html = '<div class="grid" >';
        $html .= '<table style="display:none">';
        $html .= '<tbody id="venipak_deliveryprices_template">';
        $html .= $this->_getRowTemplateHtml();
        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '<table class="border" cellspacing="0" cellpadding="0">';
        $html .= '<tbody id="venipak_deliveryprices_container">';
        $html .= '<tr class="headings">';
        $html .= '<th>'.$this->__('Country').'</th>';
        $html .= '<th>'.$this->__('Price').'</th>';
        $html .= '<th>'.$this->__('Max weight').'</th>';
        $html .= '<th>'.$this->__('Free shipping subtotal').'</th>';
        $html .= '<th>&nbsp;</th>';
        $html .= '</tr>';
        if ($this->_getValue('country')) {
            foreach ($this->_getValue('country') as $i => $f) {
                if ($i) {
                    $html .= $this->_getRowTemplateHtml($i);
                }
            }
        }

        $html .= '</tbody>';
        $html .= '</table>';
        $html .= '</div>';
        $html .= $this->_getAddRowButtonHtml('venipak_deliveryprices_container', 'venipak_deliveryprices_template', $this->__('Add combination'));

        return $html;
    }

    protected function _getRowTemplateHtml($i=0)
    {
        $html = '<tr>';
        $html .= '<td>'.$this->getCountyListHtml($this->getElement()->getName().'[country][]', $this->_getValue('country/'.$i)).'</td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[price][]" value="'.$this->_getValue('price/'.$i).'" style="width:60px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[weight][]" value="'.$this->_getValue('weight/'.$i).'" style="width:100px;" /></td>';
        $html .= '<td><input type="text" name="'.$this->getElement()->getName().'[free_subtotal][]" value="'.$this->_getValue('free_subtotal/'.$i).'" style="width:50px;" /></td>';
        $html .= '<td>'.$this->_getRemoveRowButtonHtml().'</td>';
        $html .= '</tr>';

        return $html;
    }

    protected function getRegionsListHtml($name, $value, $country)
    {
        $regions = Mage::helper('venipak/data')->getRegionsList($country);
        $html = '<select name="'.$name.'" style="width: 250px">';
        foreach ($regions as $key => $region) {
            $html .= '<option value="'.$region['value'].'"'.(($region['value']==$value)?'selected="selected"':'').'>'.$region['label'].'</option>';
        }
        $html .= '</select>';

        return $html;
    }

    protected function getCountyListHtml($name, $value)
    {
        $view_id = Mage::app()->getRequest()->getParam('store');
        if (empty($value)) {
            $value = Mage::getStoreConfig('general/country/default', $view_id);
        }
        $countrys = Mage::helper('venipak/data')->getCountyList();
        $html = '<select name="'.$name.'" style="width: 250px">';
        foreach ($countrys as $key => $country) {
            $html .= '<option value="'.$country['value'].'"'.(($country['value']==$value)?'selected="selected"':'').'>'.$country['label'].'</option>';
        }
        $html .= '</select>';

        return $html;
    }

    protected function _getDisabled()
    {
        return $this->getElement()->getDisabled() ? ' disabled' : '';
    }

    protected function _getValue($key)
    {
        return $this->getElement()->getData('value/'.$key);
    }

    protected function _getAddRowButtonHtml($container, $template, $title='Add')
    {
        if (!isset($this->_addRowButtonHtml[$container])) {
            $this->_addRowButtonHtml[$container] = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('add '.$this->_getDisabled())
            ->setLabel($this->__($title))
            ->setOnClick("Element.insert($('".$container."'), {bottom: $('".$template."').innerHTML})")
            ->setDisabled($this->_getDisabled())
            ->toHtml();
        }

        return $this->_addRowButtonHtml[$container];
    }

    protected function _getRemoveRowButtonHtml($selector='tr', $title='Delete')
    {
        if (!$this->_removeRowButtonHtml) {
            $this->_removeRowButtonHtml = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('delete v-middle '.$this->_getDisabled())
            ->setLabel($this->__($title))
            ->setOnClick("Element.remove($(this).up('".$selector."'))")
            ->setDisabled($this->_getDisabled())
            ->toHtml();
        }

        return $this->_removeRowButtonHtml;
    }
}
