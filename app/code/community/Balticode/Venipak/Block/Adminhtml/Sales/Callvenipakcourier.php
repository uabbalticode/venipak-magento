<?php

class Balticode_Venipak_Block_Adminhtml_Sales_Callvenipakcourier extends Mage_Adminhtml_Block_Template
{
    public function _construct(){
        $this->setTemplate('venipak/sales/callvenipakcourier.phtml');
    }

    protected function _prepareLayout()
    {
        $this->_injectCalendarControlJsCSSInHTMLPageHead();
        return parent::_prepareLayout();
    }

    private function _injectCalendarControlJsCSSInHTMLPageHead()
    {
        $this->getLayout()->removeOutputBlock('root') //Remove all content
            ->addOutputBlock('head') //Add head block
            ->addOutputBlock('call_venipak_courier'); //Add Venipak call courier block

        $this->getLayout()->getBlock('head')->removeItem('js', 'calendar/calendar.js')
            ->removeItem('js', 'calendar/calendar-setup.js')
            ->append(
            $this->getLayout()->createBlock(
                'Mage_Core_Block_Html_Calendar',
                'html_calendar',
                array('template' => 'page/js/calendar.phtml')
            )
        );

        $this->getLayout()->getBlock('head')
            ->addItem('js_css', 'balticode/calendar/calendar-win2k-1.css')
            ->addItem('js_css', 'balticode/callvenipakcourier.css')
            ->addJs('balticode/calendar/calendar.js')
            ->addJs('balticode/calendar/calendar-setup.js');

        return $this;
    }

    public function getCourierArrive()
    {
        $day = Mage::getModel('core/date')->date('Y/m/d');
        $data = Mage::getModel('venipak/courierdata')->addDbWhere('pickup_date = "'.$day.'"')->getData();
        return $data;
    }

    public function getWarehouseData($warehouse_id)
    {
        return Mage::helper('venipak/data')->getWarehouseData($warehouse_id);
    }

    public function getTimeInterval($start,$stop,$step = 1)
    {
        $array = array();
        for ($i = $start; $i <= $stop; $i += $step) {
            $array[] = $i;
        }

        return $array;
    }

    public function getTodayOrdersWeight()
    {
        $collection = Mage::getResourceModel('sales/order_collection');
        $now = Mage::getModel('core/date')->gmtDate(time());
        $dateStart = date('Y-m-d' . ' 00:00:00', $now);
        $dateEnd = date('Y-m-d' . ' 23:59:59', $now);
        $collection->addFieldToFilter('created_at', array('from' => date($dateStart), 'to' => date($dateEnd)));
        $collection->load();
        $orders = $collection->getData();
        $common_weight = 0;
        $availible_methods = Mage::helper('venipak/data')->_venipakMethods;
        foreach ($orders as $key => $order) {
            if (in_array($order['shipping_method'],$availible_methods)) {
                $availible_orders[$key] = $order;
                $common_weight += $order['weight'];
            }
        }
        if (!$common_weight) {
            return 1;
        }
        return $common_weight;
    }
    public function now($attribute = null)
    {
        $date = new Zend_Date(Mage::getModel('core/date')->timestamp(time(), false, Mage::app()->getLocale()->getLocaleCode()));

        return $date->toString($attribute);
    }

    public function gerOrdersIds()
    {
        $ordersList = $this->getRequest()->getParam('order_ids');
        $ordersList = explode(',', $ordersList);
        return $ordersList;
    }

    public function hasDifferenceLogins()
    {
        $message = null;
        if (!$this->loginValidation($this->gerOrdersIds())) {
            $message = Mage::helper('venipak/data')->__('Selected order has difference logins!');
        }
        return $message;
    }


    protected function loginValidation($orders)
    {
        $store_ids = array();
        foreach ($orders as $order) {
            if (!empty($order) && is_numeric($order)) {
                $order = Mage::getModel('sales/order')->load($order);
                $store_ids[] = $order->getStoreId();
            } else {
                $store_ids[] = null;
            }
        }
        //Orders store id's
        $store_ids = array_unique($store_ids);

        //Collect login data by store id
        foreach ($store_ids as $store_id) {
            $api_name[] = Mage::helper('venipak/data')->getConfigData('username', $store_id);
            $api_pass[] = Mage::helper('venipak/data')->getConfigData('password', $store_id);
            $id[] = Mage::helper('venipak/data')->getConfigData('id', $store_id);
            $api_url[] = Mage::helper('venipak/data')->getConfigData('api', $store_id);
        }

        //Make unique data
        $api_name = array_unique($api_name);
        $api_pass = array_unique($api_pass);
        $id = array_unique($id);
        $api_url = array_unique($api_url);
        $store_id = null;
        // Test logins
        if (count($api_name) == 1
            && count($api_pass) == 1
            && count($id) == 1
            && count($api_url) == 1
        ) {
            $this->api_name = Mage::helper('venipak/data')->getConfigData('username', $store_id);
            $this->api_pass = Mage::helper('venipak/data')->getConfigData('password', $store_id);
            $this->id = Mage::helper('venipak/data')->getConfigData('id', $store_id);
            $this->api_url = Mage::helper('venipak/data')->getConfigData('api', $store_id);
        } else {
            return false;
        }
        return true;
    }
}
