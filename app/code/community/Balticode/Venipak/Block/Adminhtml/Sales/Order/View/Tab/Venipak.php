<?php

class Balticode_Venipak_Block_Adminhtml_Sales_Order_View_Tab_Venipak
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected $_chat = null;

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('venipak/sales/order/view/tab/venipak.phtml');
    }

    protected function _prepareLayout()
    {
        $this->setChild('saveButton',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('adminhtml')->__('Save'),
                    'onclick'   => 'venipakForm.submit();return false;',
                    'class' => 'save'
                ))
        );

        return parent::_prepareLayout();
    }

    public function getTabLabel()
    {
        return $this->__('Venipak');
    }

    public function getTabTitle()
    {
        return $this->__('Venipak');
    }

    public function canShowTab()
    {
        if (Mage::getModel('venipak/data')->isEnabled()) { //If module is enabled
            if (Mage::helper('venipak/data')->isVenipakMethod($this->getOrder())) { //Shipping method is Venipak
                return true;
            }
        }
        return false;
    }

    public function isHidden()
    {
        if (!$this->getOrder()->getShippingAddress()) { //Is set Shipping adress? Lets show just hide
            return true;
        }
        return false;
    }

    public function getOrder(){
        return Mage::registry('current_order');
    }

    public function getSaveButtonHtml()
    {
        return $this->getChildHtml('saveButton');
    }

    public function getLabelTypeHtml()
    {
        $order = $this->getOrder();
        $label = Mage::helper('venipak/data')->getOrderValue('label_size', $order);
        $type_labels = Mage::getModel('venipak/system_config_source_labeltype')->toArray();
        $sent = Mage::helper('venipak/data')->getOrderValue('sent', $order); //Test data alert sent or not?
        if ($sent) {
            $html = $type_labels[$label];
        } else {
            $types = Mage::getModel('venipak/system_config_source_labeltype')->toOptionArray();
            $html = '<select name="label_size">';
            foreach ($types as $type) {
                if ($type['value'] == $label) {
                    $selected = ' selected="selected" ';
                } else {
                    $selected = '';
                }
                $html .= '<option value="'.$type['value'].'" '.$selected.'>'.$type['label'].'</option>';
            }
            $html .= '</select>';
        }
        return $html;
    }

    public function getWarehouseHtml()
    {
        $order = $this->getOrder();
        $data = Mage::helper('venipak/data')->getOrderValue('warehouse_id', $order);
        $helper = Mage::helper('venipak/data');
        $list = $helper->getWarehouseList($order->getStoreId());
        $list = $helper->flipWarehouseList($list);
        $sent = Mage::helper('venipak/data')->getOrderValue('sent', $order); //Test data alert sent or not?
        $html = '';
        if ($sent) {
           $html = $list[$data]['c_name'].' / '.$list[$data]['address'];
        } else {
            $html = '<select name="warehouse_id">';
            foreach ($list as $key => $value) {
                $html .= '<option value="'.$key.'" '.(($key==$data)?' selected="selected" ':'').' >'.$value['c_name'].' / '.$value['address'].', '.$value['c_city'].'</option>';
            }
            $html .= '</select>';
        }

        return $html;
    }

    public function getNoYesHtml($name)
    {
        $order = $this->getOrder();
        $data = Mage::helper('venipak/data')->getOrderValue($name, $order);
        $sent = Mage::helper('venipak/data')->getOrderValue('sent', $order); //Test data alert sent or not?
        if ($sent) {
            $html = ($data=="1")?__('Yes'):__('No');
        } else {
            $html = '<select name="'.$name.'">';
            $html .= '<option value="0"'.(($data=="0")?' selected="selected" ':'').'>'.__('No').'</option>';
            $html .= '<option value="1"'.(($data=="1")?' selected="selected" ':'').'>'.__('Yes').'</option>';
            $html .= '</select>';
        }

        return $html;
    }

    public function getManifestNrHtml()
    {
        $order = $this->getOrder();
        $manifest_nr = Mage::helper('venipak/data')->getOrderValue('manifest_nr', $order);
        if (empty($manifest_nr)) {
            $manifest_nr = Mage::helper('venipak/data')->getManifestNr($order);
        }

        return $manifest_nr.'<input type="hidden" name="manifest_nr" value="'.$manifest_nr.'" />';
    }

    public function getOfficeNrHtml()
    {
        $office_nr = Mage::helper('venipak/data')->getOrderValue('office_nr', $this->getOrder());
        return $this->getImputHtml('office_nr',$office_nr);
    }

    public function getWarehouseNrHtml()
    {
        $client_warehouse = Mage::helper('venipak/data')->getOrderValue('client_warehouse', $this->getOrder());
        return $this->getImputHtml('client_warehouse',$client_warehouse);
    }

    public function getDoorNrHtml()
    {
        $door_nr = Mage::helper('venipak/data')->getOrderValue('door_nr', $this->getOrder());
        return $this->getImputHtml('door_nr',$door_nr);
    }

    public function getPacksCountHtml()
    {
        $order = $this->getOrder();
        $pack_count = Mage::helper('venipak/data')->getPackCount($order);
        return $this->getImputHtml('pack_count',$pack_count);
    }

    public function getImputHtml($name, $value = "")
    {
        $sent = Mage::helper('venipak/data')->getOrderValue('sent', $this->getOrder()); //Test data alert sent or not?
        if ($sent) {
            $html = $value;
        } else {
            $html = '<input name="'.$name.'" value="'.$value.'" />';
        }

        return $html;
    }

    public function getPacksNrHtml()
    {
        $html = '';
        $order = $this->getOrder();
        $packs = unserialize(Mage::helper('venipak/data')->getOrderValue('pack_no', $order));
        foreach ((array)$packs as $pack) {
            $html .= $pack.' ';
        }

        return $html;
    }
}
