<?php

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS `{$installer->getTable('balticode_venipak_order_data')}`;
CREATE TABLE `{$installer->getTable('balticode_venipak_order_data')}` (
  `orderdata_id` int(11) unsigned NOT NULL auto_increment,
  `entity_id` int(10) unsigned NOT NULL,
  `label_size` varchar(255) NULL,
  `pack_count` varchar(255) NULL,
  `warehouse_id` varchar(255) NULL default '1',
  `client_warehouse` varchar(255) NULL,
  `return_doc` TINYINT(1) NULL,
  `manifest_nr` varchar(255) NULL,
  `pack_no` text NULL,
  `time_stamp` varchar(255) NULL,
  `office_nr` varchar(255) NULL,
  `door_nr` varchar(255) NULL,
  `comment_call` TINYINT(1) NULL,
  `sent` TINYINT(1) NULL,

PRIMARY KEY (`orderdata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

$installer->run("
DROP TABLE IF EXISTS `{$installer->getTable('balticode_venipak_courier_data')}`;
CREATE TABLE `{$installer->getTable('balticode_venipak_courier_data')}` (
  `courierdata_id` int(11) unsigned NOT NULL auto_increment,
  `weight` varchar(255) NOT NULL,
  `volume` varchar(255) NOT NULL,
  `pickup_date` varchar(255) NOT NULL,
  `pickup_time_from` varchar(255) NULL,
  `pickup_time_to` varchar(255) NULL,
  `warehouse_id` int(11) NOT NULL,
  `spp` varchar(255) NULL,
  `doc_no` varchar(255) NULL,
  `comment` varchar(50) NULL,
  `code` int(11) unsigned NULL,

PRIMARY KEY (`courierdata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
// $installer->run("
// DROP TABLE IF EXISTS `{$installer->getTable('balticode_venipak_order_data')}`;
// CREATE TABLE `{$installer->getTable('balticode_venipak_order_data')}` (
//   `orderdata_id` int(11) unsigned NOT NULL auto_increment,
//   `entity_id` int(10) unsigned NOT NULL,
//   `label_size` varchar(255) NULL,
//   `pack_count` varchar(255) NULL,
//   `warehouse_id` varchar(255) NULL default '1',
//   `return_doc` TINYINT(1) NULL,
//   `manifest_nr` varchar(255) NULL,
//   `pack_no` text NULL,
//   `time_stamp` varchar(255) NULL,
//   `office_nr` varchar(255) NULL,
//   `door_nr` varchar(255) NULL,
//   `comment_call` TINYINT(1) NULL,
//   `sent` TINYINT(1) NULL,

// PRIMARY KEY (`orderdata_id`),FOREIGN KEY(entity_id) REFERENCES sales_flat_order(entity_id)
// ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
//");

// $constraints = array(
//     'balticode_venipak_order_data' => array(
//         'parent' => array('entity_id', 'sales_flat_order', 'entity_id'),
//       ),
//     );


// foreach ($constraints as $table => $list) {
//     foreach ($list as $code => $constraint) {
//         $constraint[1] = $installer->getTable($constraint[1]);
//         array_unshift($constraint, $installer->getTable($table));
//         array_unshift($constraint, strtoupper($table . '_' . $code));

//         call_user_func_array(array($installer->getConnection(), 'addConstraint'), $constraint);
//     }
// }

$installer->endSetup();

?>