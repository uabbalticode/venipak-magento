<?php

class Balticode_Venipak_Model_Observer
{
    public function addVenipakAction($observer)
    {
        if (Mage::getModel('venipak/data')->isEnabled()) {
            $block = $observer->getEvent()->getBlock();
            if (get_class($block) =='Mage_Adminhtml_Block_Widget_Grid_Massaction'
                && $block->getRequest()->getControllerName() == 'sales_order')
            {
                $block->addItem('venipakshipment', array(
                    'label' => Mage::helper('venipak')->__('Print Venipak label'),
                    'url' => Mage::app()->getStore()->getUrl('balticode_venipak/adminhtml_xml/ShipmentXml'),
                ));
                $block->addItem('venipakshipmentbill', array(
                    'label' => Mage::helper('venipak')->__('Print Venipak Manifest'),
                    'url' => Mage::app()->getStore()->getUrl('balticode_venipak/adminhtml_xml/OrderXml'),
                ));
            }

            if (get_class($block) == 'Mage_Adminhtml_Block_Sales_Order'
                && $block->getRequest()->getControllerName() == 'sales_order') {
                $onClickJsFunctions = 'callVenipakCourier(\''.
                        Mage::helper('adminhtml')->getUrl('balticode_venipak/adminhtml_order/call/').'\',\''.
                        Mage::helper('venipak')->__('Call Venipak Courier').'\')';

                if ($this->_orderIsRequired()) {
                    $onClickJsFunctions = 'if(validateOrdersId(\'order_ids\')) {'.$onClickJsFunctions.'}';
                }

                $block->addButton('call', array(
                    'label'     => Mage::helper('venipak')->__('Call Venipak courier'),
                    'onclick'   => $onClickJsFunctions,
                    'class'     => 'callVenipakCourier',
                ));
            }
        }
    }

    private function _orderIsRequired()
    {
        $collection = Mage::getModel('core/config_data')
            ->getCollection()
            ->addFieldToSelect('value')
            ->addFieldToSelect('path')
            ->addFieldToFilter(
                array('path', 'path', 'path'),
                array(
                    array('eq' => 'carriers/balticode_venipak/username'),
                    array('eq' => 'carriers/balticode_venipak/password'),
                    array('eq' => 'carriers/balticode_venipak/api'),
                    array('eq' => 'carriers/balticode_venipak/id'),
                )
            );
        $loginData = array();
        foreach ($collection->getData() as $value) {
            $loginData[$value['path']][] = $value['value'];
        }
        foreach ($loginData as $path => $values) {
            $uniqueValues = array_unique($values);
            if (count($uniqueValues) > 1) {
                return true;
            }
        }
        return false;
    }

    public function saleOrderViewShippingInformation(Varien_Event_Observer $observer)
    {
        $shipping_method = $observer->getEvent()->getShippingInformation()->getShippingMethod();

        if (in_array($shipping_method, Mage::helper('venipak')->_venipakMethods)) {
            $orderId = $observer->getEvent()->getShippingInformation()->getId();

            $shipingInformation = $observer->getEvent()->getShippingInformation();
            //Get current description of carrier
            $currentDescription = $shipingInformation->_getShippingDescription();
            //Get description of own carrier method

            $venipakOrderData = Mage::getModel('venipak/orderdata')->getRowById($orderId);

            $delivery_time_code = $venipakOrderData->getData('time_stamp');
            if (isset($delivery_time_code)) {
                $delivery_time_label = Mage::helper('venipak/data')->getDeliveryTimeLabel($delivery_time_code);
                $currentDescription .= " / ".Mage::helper('venipak')->__('Delivery time: ').$delivery_time_label;
            }
            //Set description
            $shipingInformation->_setShippingDescription($currentDescription);
        }

        return $this;
    }

    public function preDispatch(Varien_Event_Observer $observer)
    {
        if (Mage::getSingleton('admin/session')->isLoggedIn()) {
            $feedModel  = Mage::getModel('Balticode_Venipak_Model_Feed');
            $feedModel->checkUpdate();
        }
    }

    public function saveShippingMethod($evt)
    {
        $request = $evt->getRequest();
        $quote = $evt->getQuote();
        $venipak = $request->getParam('shipping_venipak', false);
        $quote_id = $quote->getId();

        $data = array('entity_id' => $quote_id,
            'client_warehouse' => (isset($venipak['client_warehouse'])?$venipak['client_warehouse']:null),
            'time_stamp' => (isset($venipak['time'])?$venipak['time']:null),
            'office_nr' => (isset($venipak['office'])?$venipak['office']:null),
            'door_nr' => (isset($venipak['door'])?$venipak['door']:null),
            'comment_call' => (isset($venipak['call'])?$venipak['call']:null),
            'sent'=> '0'
        );

        Mage::getModel('venipak/orderdata')->updateDbRow($quote_id, $data, true);
    }

    public function saveOrderAfter($evt)
    {
        $order = $evt->getOrder();
        $quote = $evt->getQuote();
        $quote_id = $quote->getId();
        $venipak = Mage::getSingleton('venipak/orderdata')->load($quote_id);
        if (isset($venipak)) {
            $data = array(
                'entity_id' => $order->getId(), //This is Important!!!
                'label_size' => Mage::helper('venipak/data')->getOrderValue('label_size', $order),
                'pack_count' => Mage::helper('venipak/data')->getPackCount($order),
                'warehouse_id' => Mage::helper('venipak/data')->default_warehouse_id,
                'return_doc' => Mage::helper('venipak/data')->getOrderValue('return_doc', $order),
                );
            $venipakModel = Mage::getSingleton('venipak/orderdata')->updateDbRow($quote_id,$data,true);
        }
        if (isset($venipak[$quote_id])) {
            $data = $venipak[$quote_id];
            $data['order_id'] = $order->getId();
            $venipakModel = Mage::getModel('venipak/orderdata');
            $venipakModel->setData($data);
            $venipakModel->save();
        }
    }

    public function loadOrderAfter($evt)
    {
        $order = $evt->getOrder();
        if ($order->getId()) {
            $order_id = $order->getId();
            $venipak = Mage::getModel('venipak/orderdata')->getRowById($order_id);
            $order->setVenipakObject($venipak);
        }
    }

    public function loadQuoteAfter($evt)
    {
        $quote = $evt->getQuote();
        if ($quote->getId()) {
            $quote_id = $quote->getId();
            $venipak = Mage::getSingleton('checkout/session')->getVenipak();
            if (isset($venipak[$quote_id])) {
                $data = $venipak[$quote_id];
                $quote->setVenipakData($data);
            }
        }
    }

    /**
     * Set tracking Number to Shipment
     * @param  [type] $observer [description]
     * @return [type]           [description]
     */
    public function salesOrderShipmentSaveAfter($observer)
    {
        $shipment = $observer->getEvent()->getShipment();
        $order = $shipment->getOrder();
        $carrier_code = Mage::getModel('venipak/carrier')->getCode();

        if (strpos($order->getShippingMethod(), $carrier_code) !== false) {
            $trackingNumbers = unserialize(
                Mage::helper('venipak/data')->getOrderValue('pack_no', $order)
            );

            $shipmentTrack = Mage::getModel('sales/order_shipment_track');
            $trackingData = $shipmentTrack
                ->getCollection()
                ->addFieldToFilter('order_id', $order->getEntityId())
                ->getData();

            $trackingUrl = Mage::helper('venipak/api')->getConfigData('api')
                .Mage::getSingleton('venipak/carrier')->getTrackingUrl();

            foreach ($trackingNumbers as $trackingNumber) {
                $found = false;

                if (!empty($trackingData)) {
                    foreach ($trackingData as $trackingLine) {
                        if (strpos($trackingLine['track_number'], trim($trackingNumber)) !== false) {
                            $found = true;
                            break;
                        }
                    }
                }

                if (!$found) {
                    $url = null;
                    if ($trackingUrl != null) {
                        $url = sprintf($trackingUrl, trim($trackingNumber));
                    }
                    $track = Mage::getModel('sales/order_shipment_track')
                        ->setShipment($shipment)
                        ->setParentId($shipment->getEntityId())
                        ->setTrackNumber($url)
                        ->setNumber($url)
                        ->setCarrierCode($carrier_code)
                        ->setTitle(Mage::getSingleton('venipak/carrier')->getConfigData('title'))
                        ->setOrderId($shipment->getData('order_id'));
                    $track->save();
                }
            }
        }
    }
}
