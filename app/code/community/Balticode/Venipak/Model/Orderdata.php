<?php

class Balticode_Venipak_Model_Orderdata extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('venipak/orderdata');
    }

    public function updateDbRow($entity_id, $data, $create = false)
    {
        $isset = count($this->load($entity_id)->getData());
        if ($isset){ //if found then update
            $this->getResource()->updateRow($entity_id,$data);
        } elseif ($create) { //if not found add new line
            $data['entity_id'] = $entity_id;
            $this->getResource()->addRow($data);
        }
        return $this;
    }

    public function getRowById($entity_id)
    {
        return $this->load($entity_id);
    }
}
