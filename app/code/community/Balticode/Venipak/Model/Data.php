<?php

class Balticode_Venipak_Model_Data extends Mage_Core_Model_Abstract
{
    private $_debug = false;

    protected function _construct()
    {
        $this->_init('balticode_venipak/orderdata');
    }

    /**
     * Enabled or Disabled module
     *
     * @return Boolean
     */
    public function isEnabled()
    {
        if (Mage::helper('venipak/data')->getConfigData("enabled")) {
            return true;
        }
        return false;
    }

    /**
     * If enabled write Error or Warning to log File
     *
     * @return Boolean
     */
    public function writeError($message, $file_name = "venipak.log")
    {
        if (Mage::helper('venipak/data')->getConfigData("log")) {
            Mage::Log($message, null, $file_name);
        }
    }

    /**
     * If set true on $this->_debug so write Debug data to log File
     *
     * @return Boolean
     */
    public function writeDebug($message, $file_name = "venipak.log")
    {
        if ($this->_debug) {
            Mage::Log($message, null, $file_name);
        }
    }

    /**
     * Funciton convert Object to Array Recursive
     *
     * @param Object
     * @return Array
     */
    public function objectToArray($d)
    {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return array_map(array($this, __FUNCTION__), $d);
        } else {
            // Return array
            return $d;
        }
    }
}
