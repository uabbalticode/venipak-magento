<?php

class Balticode_Venipak_Model_System_Config_Source_Packtype
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'order', 'label' => Mage::helper('adminhtml')->__('Per order')),
            array('value' => 'item', 'label' => Mage::helper('adminhtml')->__('Per items')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'order' => Mage::helper('adminhtml')->__('Per order'),
            'item' => Mage::helper('adminhtml')->__('Per items'),
        );
    }
}
