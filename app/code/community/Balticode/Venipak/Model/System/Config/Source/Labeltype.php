<?php

class Balticode_Venipak_Model_System_Config_Source_Labeltype
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'a4', 'label' => Mage::helper('adminhtml')->__('A4')),
            array('value' => '100x150', 'label' => Mage::helper('adminhtml')->__('100x150')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'a4' => Mage::helper('adminhtml')->__('A4'),
            '100x150' => Mage::helper('adminhtml')->__('100x150'),
        );
    }
}
