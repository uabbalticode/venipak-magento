<?php

class Balticode_Venipak_Model_System_Config_Source_Availablefields
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'time', 'label' => Mage::helper('adminhtml')->__('Shipping time')),
            array('value' => 'door', 'label' => Mage::helper('adminhtml')->__('Door code')),
            array('value' => 'office', 'label' => Mage::helper('adminhtml')->__('Office room number')),
            array('value' => 'warehouse', 'label' => Mage::helper('adminhtml')->__('Warehouse number')),
            array('value' => 'call', 'label' => Mage::helper('adminhtml')->__('Call before delivery'))
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'time' => Mage::helper('adminhtml')->__('Shipping time'),
            'door' => Mage::helper('adminhtml')->__('Door code'),
            'office' => Mage::helper('adminhtml')->__('Office room number'),
            'warehouse' => Mage::helper('adminhtml')->__('Warehouse number'),
            'call' => Mage::helper('adminhtml')->__('Call before delivery'),
        );
    }
}
