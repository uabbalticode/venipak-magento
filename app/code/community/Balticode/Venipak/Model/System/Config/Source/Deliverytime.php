<?php

class Balticode_Venipak_Model_System_Config_Source_Deliverytime
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 'nwd', 'label' => Mage::helper('adminhtml')->__('delivery next working day')),
            array('value' => 'nwd10', 'label' => Mage::helper('adminhtml')->__('delivery next working day till 10:00')),
            array('value' => 'nwd12', 'label' => Mage::helper('adminhtml')->__('delivery next working day till 12:00')),
            array('value' => 'nwd8_14', 'label' => Mage::helper('adminhtml')->__('delivery next working day 8:00-14:00')),
            array('value' => 'nwd14_17', 'label' => Mage::helper('adminhtml')->__('delivery next working day 14:00-17:00')),
            array('value' => 'nwd18_20', 'label' => Mage::helper('adminhtml')->__('delivery next working day 18:00-22:00')),
            array('value' => 'nwd18a', 'label' => Mage::helper('adminhtml')->__('delivery next working day after 18:00')),
            array('value' => 'sat', 'label' => Mage::helper('adminhtml')->__('delivery on saturday')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'nwd' => Mage::helper('adminhtml')->__('delivery next working day'),
            'nwd10' => Mage::helper('adminhtml')->__('delivery next working day till 10:00'),
            'nwd12' => Mage::helper('adminhtml')->__('delivery next working day till 12:00'),
            'nwd8_14' => Mage::helper('adminhtml')->__('delivery next working day 8:00-14:00'),
            'nwd14_17' => Mage::helper('adminhtml')->__('delivery next working day 14:00-17:00'),
            'nwd18_20' => Mage::helper('adminhtml')->__('delivery next working day 18:00-22:00'),
            'nwd18a' => Mage::helper('adminhtml')->__('delivery next working day after 18:00'),
            'sat' => Mage::helper('adminhtml')->__('delivery on saturday'),
        );
    }
}
