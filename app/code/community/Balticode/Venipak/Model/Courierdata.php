<?php

class Balticode_Venipak_Model_Courierdata extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('venipak/courierdata');
    }

    public function addDbRow($post_data)
    {
        $data = array(
            'weight' => $post_data['weight'],
            'volume' => $post_data['volume'],
            'pickup_date' => $post_data['date'],
            'pickup_time_from' => $post_data['time_from_h'].':'.$post_data['time_from_m'],
            'pickup_time_to' => $post_data['time_to_h'].':'.$post_data['time_to_m'],
            'warehouse_id' => $post_data['warehouse_id'],
            'spp' => $post_data['spp'],
            'doc_no' => $post_data['doc_no'],
            'comment' => $post_data['comment'],
        );

        return $this->getResource()->addRow($data);
    }

    public function getDbLastId()
    {
        return $this->getResource()->getLastId();
    }

    public function updateDbRow($courierdata_id, $data, $create = false)
    {
        $isset = count($this->load($courierdata_id)->getData());
        if ($isset) { //if found then update
            $this->getResource()->updateRow($courierdata_id,$data);
        } elseif ($create) { //if not found, add new line
            $data['courierdata_id'] = $courierdata_id;
            $this->getResource()->addRow($data);
        }

        return $this;
    }

    public function getDbData($column = '*')
    {
        return $this->getResource()->getData($column);
    }

    public function addDbWhere($where)
    {
        return $this->getResource()->addWhere($where);
    }

    public function removeDbRow($courierdata_id)
    {
        return $this->getResource()->removeRow($courierdata_id);
    }
}
