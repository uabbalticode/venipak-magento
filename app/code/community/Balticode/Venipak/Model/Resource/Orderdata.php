<?php

class Balticode_Venipak_Model_Resource_Orderdata extends Mage_Core_Model_Mysql4_Abstract
{
 
    protected function _construct()
    {
        $this->_init('venipak/orderdata', 'entity_id');
    }

    public function addRow($data)
    {
        $this->_getWriteAdapter()->insert($this->getMainTable(), $data);
        return $this;
    }

    public function updateRow($entity_id, $data)
    {
        $table = $this->getMainTable();
        $where = $this->_getWriteAdapter()->quoteInto('entity_id = ?', $entity_id);
        return $this->_getWriteAdapter()->update($table, $data, $where); 
    }
}
