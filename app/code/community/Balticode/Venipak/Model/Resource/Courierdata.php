<?php

class Balticode_Venipak_Model_Resource_Courierdata extends Mage_Core_Model_Mysql4_Abstract
{
    private $_where;

    protected function _construct()
    {
        $this->_init('venipak/courierdata', 'courierdata_id');
    }

    public function addRow($data)
    {
        $this->_getWriteAdapter()->insert($this->getMainTable(), $data);
        return $this;
    }

    public function getLastId()
    {
        $primatyKeyName = $this->getIdFieldName();
        $tableName = $this->getMainTable();
        $result = $this->_getReadAdapter()->raw_fetchRow("SELECT MAX(`{$primatyKeyName}`) as LastID FROM `{$tableName}`");
        return $result['LastID'];
    }

    public function updateRow($courierdata_id, $data)
    {
        $table = $this->getMainTable();
        $where = $this->_getWriteAdapter()->quoteInto('courierdata_id = ?', $courierdata_id);
        return $this->_getWriteAdapter()->update($table, $data, $where);
    }


    public function getData($column = '*')
    {
        $select = $this->_getReadAdapter()->select()->from($this->getMainTable(), $column);
        if (!empty($this->_where)) {
            $select->where($this->_where);
        }
        $result = $this->_getReadAdapter()->fetchAll($select,array('courierdata_id'));
        return $result;
    }

    public function addWhere($where)
    {
        $this->_where = $where;
        return $this;
    }

    public function removeRow($courierdata_id)
    {
        $adapter = $this->_getWriteAdapter();

        $adapter->delete(
            $this->getMainTable(),
            array('courierdata_id = ?' => $courierdata_id)
        );

        return $adapter;
    }

}
