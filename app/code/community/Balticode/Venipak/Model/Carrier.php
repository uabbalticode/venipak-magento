<?php

class Balticode_Venipak_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
    implements Mage_Shipping_Model_Carrier_Interface
{
    protected $_code = 'balticode_venipak';
    protected $_tracking_url = 'ws/tracking?code=%s&type=1';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!Mage::getStoreConfig('carriers/'.$this->_code.'/active')
            || !Mage::getModel('venipak/data')->isEnabled()) {
            return false;
        }
        $price = Mage::helper('venipak/data')->getShippingPrice(
            $request->getData('dest_country_id'),
            $request->getData('dest_region_code'),
            $request->getData('package_weight'),
            $request->getData('package_value_with_discount')
        );

        if ($request->getFreeShipping() === true) {
            $price = '0.00';
        }

        if ($price === false) {
            return false;
        }

        $handling = Mage::getStoreConfig('carriers/'.$this->_code.'/handling');
        $result = Mage::getModel('shipping/rate_result');
        $method = Mage::getModel('shipping/rate_result_method');
        $method->setCarrier($this->_code);
        $method->setMethod($this->_code);
        $method->setCarrierTitle($this->getConfigData('title'));
        $method->setMethodTitle($this->getConfigData('name'));
        $method->setPrice($price);
        $method->setCost($price);
        $result->append($method);
        return $result;
    }

    public function getAllowedMethods()
    {
        return array('balticode' => $this->getConfigData('name'));
    }

    /**
     * If carrier supports external tracking URL's then it should return true
     * @return boolean
     */
    public function isTrackingAvailable()
    {
        if ($this->getTrackingUrl()) {
            return true;
        }
        return false;
    }

    public function getFormBlock()
    {
        return 'venipak/venipak';
    }

    /**
     * <p>Returns tracking URL for current carrier if one exists.</p>
     * @return bool|string
     */
    public function getTrackingUrl()
    {
        return $this->_tracking_url;
    }

    public function getCode()
    {
        return $this->_code;
    }

    public function getTrackingInfo($number)
    {
        if (strpos($number, 'http://') !== false) { //if in number is full URL address
            return array(
                'title' => $this->getConfigData('title'),
                'number' => $number,
            );
        } else { //if number is just tracking number
            $number = str_replace('?', '_', $number); //replace question-mark to underscore, because to Zend not like '?'
            $track = Mage::getModel('sales/order_shipment_track')
                ->getCollection()
                ->addFieldToFilter('track_number', array('like' => $number)) //Search existing number
                ->addFieldToSelect('*') //Select all
                ->getFirstItem(); //grab first, because cant be only one with this number

            if ($track->hasData()) {
                $trackingUrl = Mage::helper('venipak/api')->getConfigData('api')
                    .Mage::getSingleton('venipak/carrier')->getTrackingUrl(); //Collect URL address with pattern
                $url = sprintf($trackingUrl, trim($number)); //change pattern to number

                $track->setTracking($number);
                $track->setUrl($url);
                return $track;
            } else {
                return array(
                    'title' => $this->getConfigData('title'),
                    'number' => $number,
                );
            }
        }
    }
}
