<?php

class Balticode_Venipak_Helper_Api extends Mage_Core_Helper_Abstract
{
    private $_module; // Module Name
    private $_store_id = null; //Store ID
    private $_user; // API User Name
    private $_password; // API User Password
    private $_id; // API id
    private $_api; // API URL
    private $_actions = array (
            "import_send" => "import/send.php",
            "pack_label" => "ws/print_label.php", //Get ‚Pack label‘ pdf file
            "shipment_bill" => "ws/print_list.php", //Get ‚ Sender's shipment bill ‘ pdf file
            "label_link" => "ws/print_link.php", //Make Label link to pdf file
            "tracking_by_pack_number" => "ws/tracking.php", //Tracking shipment/package by pack number
            "tracking_by_pack_document_code" => "ws/tracking.php", //Tracking package by pack document code
            "tracking_by_shipment_bill" => "ws/tracking.php", //Tracking package by „Sender's shipment bill“
            "get_route_by_post_code" => "ws/get_route.php", //GET route by post code
        );

    public function collectLogins($storeId = null)
    {
        if ($storeId == null) {
            $storeId = $this->getStoreId();
        }
        $this->_module = strtolower($this->_getModuleName());
        $this->_user = $this->getConfigData('username', $storeId);
        $this->_password = $this->getConfigData('password', $storeId);
        $this->_id = $this->getConfigData('id', $storeId);
        $this->_api = $this->getConfigData('api', $storeId);

        return $this;
    }

    public function setStore($storeId)
    {
        $this->_store_id = $storeId;
        return $this;
    }

    public function getStoreId()
    {
        return $this->_store_id;
    }

    /**
     * Retrieve information from settings configuration
     *
     * @param   string $field
     * @return  mixed
     */
    public function getConfigData($field, $store_id = null)
    {
        $path = 'carriers/'.$this->_module.'/'.$field;
        return Mage::getStoreConfig($path, $store_id);
    }

    public function sendData($xml_file = null, $xml_text = null)
    {
        $this->collectLogins();
        $post = array(
            "user" => $this->_user, // Max length 15
            "pass" => $this->_password, // Max length 15
            "xml_file" => $xml_file, // Max size 640kb
            "xml_text" => $xml_text,
            //"sandbox" => $this->getConfigData('sandbox'),
            "system_version" => Mage::helper('venipak')->getSystemVersion(),
            "module_version" => Mage::helper('venipak')->getModuleVersion(),
        );
        return $this->_connectAPI($post, "import_send");
    }

    public function getPackLabelPdfFile($pack_no = array(), $manifest_nr = null, $type)
    {
        $this->collectLogins();
        $post = array(
            "user" => $this->_user, // Max length 15
            "pass" => $this->_password, // Max length 15
            "pack_no" => $pack_no, // text or array eg. Pack number (V00000E0000001)
        //  "code" => $manifest_nr, // Sender's shipment bill number (12345131029001)
            "type" => $type, //„a4“ – page size A4 other - page size 100 X 150
            //"sandbox" => $this->getConfigData('sandbox'),
            "system_version" => Mage::helper('venipak')->getSystemVersion(),
            "module_version" => Mage::helper('venipak')->getModuleVersion(),
        );
        return $this->_connectAPI($post,"pack_label");
    }

    public function getSenderShipmentBillPdfFile($manifest_no)
    {
        $this->collectLogins();
        $post = array(
            "user" => $this->_user, // Max length 15
            "pass" => $this->_password, // Max length 15
            "code" => $manifest_no, //Sender's shipment bill number (12345131029001)
            //"sandbox" => $this->getConfigData('sandbox'),
            "system_version" => Mage::helper('venipak')->getSystemVersion(),
            "module_version" => Mage::helper('venipak')->getModuleVersion(),
        );
        return $this->_connectAPI($post,"shipment_bill");
    }

    private function _connectAPI($post, $action)
    {
        $query = http_build_query($post);
        $chanel = curl_init();
        curl_setopt($chanel, CURLOPT_URL,$this->_api.$this->_actions[$action]);
        curl_setopt($chanel, CURLOPT_POST, 1);
        curl_setopt($chanel, CURLOPT_POSTFIELDS,  $query);
        curl_setopt($chanel, CURLOPT_RETURNTRANSFER, true);
        $server_response = curl_exec ($chanel);
        $httpCode = curl_getinfo($chanel, CURLINFO_HTTP_CODE);
        curl_close ($chanel);
        if ($httpCode == 404 || $httpCode == 0) {
            $text = 'Error: Wrong Venipak API URL';
            Mage::getSingleton('adminhtml/session')->addWarning($text);
            Mage::getSingleton('venipak/data')->writeError($text."\n".'Using URL: '.$this->_api);
            return false;
        }
        return $server_response;
    }

}
