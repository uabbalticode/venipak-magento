<?php

class Balticode_Venipak_Helper_Data extends Mage_Core_Helper_Abstract
{
    public $default_warehouse_id = 1; //Notrecomend to change that
    public $warehouse_id_pad = 3; //how much simbols has warehouses for eg. 001 <- Default warehouse
    private $module = 'venipak';

    private $_codPayMentMethods = array('cashondelivery', 'Balticode_Cashondelivery');
    public $_venipakMethods = array('balticode_venipak_balticode_venipak');

    /**
     * Return Default Warehouse id
     *
     * @return Default Warehouse id
     */
    public function getWarehouseIdPad()
    {
        return $this->warehouse_id_pad;
    }

    /**
     * Generate ShipmentXML
     *
     * @return Venipak ShipmentXML
     */
    public function getShipmentXmlData($order)
    {
        $order_data = $order->getData(); //collect order data
        $warehouses = $this->flipWarehouseList($this->getWarehouseList($order->getStoreId())); //collect warehouse list
        $order_warehouse = $warehouses[$this->getWarehouseId($order)]; //get data about selected warehouse, adress, name and etc.
        $pack_count = $this->getPackCount($order); //How much packs has ordered, or set
        $shipping_data = $order->getShippingAddress()->getData(); //collect order shipping data
        $items = $order->getAllVisibleItems(); //Load all ordered items to calc wieght

        $OrdersInXml = array(
            '@attributes' => array(
                'type' => "1", //the type of data import when You indicate pack_no.
            ),
            'manifest' => array(
                '@attributes' => array(
                    'title' => $this->getManifestNr($order), //Manifest title is a number of Sender‘s shipment bill.
                ),
                'shipment' => array(),
            ),
        );
        $OrdersInXml['manifest']['shipment'] = array(
            'consignee' => array( //recipient
                'name' => $shipping_data['firstname'].' '.$shipping_data['lastname'], //Consignee name (company name, person name etc)
                'company_code' => "", //Company code
                'country' => $shipping_data['country_id'], //EE,LT,LV,PL
                'city' => $shipping_data['city'], //Consignee city/town. For example: Vilnius; Kauno raj.;
                //$shipping_data['region'].', '.$shipping_data['city'].', '.
                'address' => $shipping_data['street'], //Consignee address. For example: Parokiškio km., Liepų al. 2-53
                'post_code' => preg_replace('/[^0-9]/', '', $shipping_data['postcode']), //Consignee post code. For examle: 23456. EE-5, LT-5, LV-4, PL-5
                'contact_person' => $shipping_data['firstname'], //Contact person name to whom a courier can call
                'contact_tel' => $shipping_data['telephone'], //Contact person telephone number a courier can call
            ),
            'sender' => array( //recipient
                'name' => $order_warehouse['c_name'], //Return documents consignee
                'company_code' => $order_warehouse['c_code'], //Return documents company code.
                'country' => $order_warehouse['iso'], //Return documents consignee country:
                                    /*
                                    EE – Estonia
                                    LT – Lithuania
                                    LV – Latvia
                                    PL – Poland
                                    */
                'city' => $order_warehouse['c_city']/*$order_warehouse['city']*/, //Return documents consignee city/town. For example: Vilnius; Kauno raj.;
                'address' => $order_warehouse['address'], //Return documents consignee address. For example: Parokiškio km., Liepų al. 2-53
                'post_code' => $order_warehouse['p_code'], //Return documents consignee post code. For examle: 23456.
                'contact_person' => $order_warehouse['name'], //Contact person name
                'contact_tel' => $order_warehouse['tel'], //Contact person telephone number
            ),
            'attribute' => array(
                'shipment_code' => $order->getData('increment_id'), //Your shipment code. You can assign to any shipment any shipment code.
                'delivery_type' => $this->getOrderValue('time_stamp',$order),
                /*
                                tswd – delivery the same working day
                                nwd – delivery next working day
                                nwd10 – delivery next working day till 10:00
                                nwd12 – delivery next working day till 12:00
                                nwd8_14 – delivery next working day 8:00-14:00
                                nwd14_17 – delivery next working day 14:00-17:00
                                nwd18_20 – delivery next working day 18:00-22:00
                                nwd18a – delivery next working day after 18:00
                                sat – delivery on saturday
                                If this field is left blank „nwd“ is assigned.
                */
                'return_doc' => $this->getOrderValue('return_doc',$order), //1 – if You want a courier to return a signed document which leads a shipment
//                  'return_doc_consignee' => ($this->getOrderValue('return_doc',$order) == '1')?
//                  array( //Address for the return documents. If this address is not indicated, the document will be returned to the sender.
//                      'name' => $order_warehouse['c_name'], //Return documents consignee
//                      'company_code' => $order_warehouse['c_code'], //Return documents company code.
//                      'country' => $order_warehouse['iso'], //Return documents consignee country:
//                                          /*
//                                          EE – Estonia
//                                          LT – Lithuania
//                                          LV – Latvia
//                                          PL – Poland
//                                          */
//                      'city' => $order_warehouse['c_city']/*$order_warehouse['city']*/, //Return documents consignee city/town. For example: Vilnius; Kauno raj.;
//                      'address' => $order_warehouse['address'], //Return documents consignee address. For example: Parokiškio km., Liepų al. 2-53
//                      'post_code' => $order_warehouse['p_code'], //Return documents consignee post code. For examle: 23456.
//                      'contact_person' => $order_warehouse['name'], //Contact person name
//                      'contact_tel' => $order_warehouse['tel'], //Contact person telephone number
//                      ):array(),
                'dlr_code' => ((@class_exists('Mage_Paysera_Model_PaymentMethod'))?((@method_exists('Mage_Paysera_Model_PaymentMethod','getShippingNumber'))?Mage_Paysera_Model_PaymentMethod::getShippingNumber(12):array()):array()),
                'doc_no' => $order->getData('increment_id'), //You can assign to any shipment any document number. For example: invoice number, oder number etc.
                'cod' => $this->isCodPayment($order), //COD (cash on delivery). Exact money amount which has to be collected from a consignee.
                'cod_type' => $order->getData('order_currency_code'), //COD currency type code EUR, LTL, LVL
                'comment_door_code' => $this->getOrderValue('door_nr',$order), //Shipment comment: if consignee door has a code, you have to indicate it.
                'comment_office_no' => $this->getOrderValue('office_nr',$order), //Shipment comment: you can indicate consignee‘s office room number
                'comment_warehous_no' => $this->getOrderValue('client_warehouse',$order), //Shipment comment: you can indicate consignee‘s warehouse number
                'comment_call' => $this->getOrderValue('comment_call',$order), //1 – if you want a courier called a consignee before shipment delivery
            ),
            'pack' => array(),
        );

        if ($pack_count == $order->getData('total_item_count')) {
            foreach ($items as $item) {
                $item_data = $item->getData();
                $OrdersInXml['manifest']['shipment']['pack'][] = array(
                    'pack_no' => $this->getPackNr($order), //Pack number looks like this: V12345E1234567
                    'doc_no' => $item_data['order_id'], //You can assign to any pack any document number. For example: invoice number, order number etc.
                    'weight' => (($item_data['weight'] == 0)?"1":($item_data['weight']*$item_data['qty_ordered'])), //Weight of pack in kilogram
                    'volume' => array(), //Volume of pack in cubic meters.
                );
            }
        } else {
            $weight = 0.0000;
            foreach ($items as $item) { //Sum values of orders
                $item_data = $item->getData();
                $weight += (float)$item_data['weight']*$item_data['qty_ordered']; //Weight of pack in kilogram
                //$volume += array(), //Volume of pack in cubic meters.
            }

            $item_weight = $weight / $pack_count;
            for ($pack_count;$pack_count>0;$pack_count--) {
                $OrdersInXml['manifest']['shipment']['pack'][] = array(
                    'pack_no' => $this->getPackNr($order), //Pack number looks like this: V12345E1234567
                    'doc_no' => array(), //You can assign to any pack any document number. For example: invoice number, order number etc.
                    'weight' => (($item_weight == 0)?"1":$item_weight), //Weight of pack in kilogram
                    'volume' => array(), //Volume of pack in cubic meters.
                );
            }
        }

        $xml = $this->_convertArray2Xml($OrdersInXml);
        return $xml;

    }

    /**
     * Generate CallCourrierXML data
     *
     * @return Venipak Orders XML data
     */
    public function getCourierXmlData($courier_data_id, $selected_ordersIds)
    {
        $courier_data = Mage::getModel('venipak/courierdata')->load($courier_data_id);
        $stoerId = null;
        if (count($selected_ordersIds)) {
            $order = Mage::getModel('sales/order')->load(reset($selected_ordersIds));
            $stoerId = $order->getStoreId();
        }
        $warehouses = $this->flipWarehouseList($this->getWarehouseList($stoerId)); //collect warehouse list
        $sender_warehouse_data = $warehouses[$courier_data->getData('warehouse_id')]; //get data about selected warehouse, adress, name and etc.
        $OrdersInXml = array(); // Create empty array
        $OrdersInXml = array(
            '@attributes' => array(
                    'type' => "3", //import of orders to pick up shipment
                ),
            'sender' => array(
                'name' => $sender_warehouse_data['c_name'], //Sender‘s name
                'company_code' => $sender_warehouse_data['c_code'], //Company code.
                'country' => $sender_warehouse_data['iso'], //Sender‘s country: EE, LT LV, PL
                'city' => $sender_warehouse_data['c_city'], //Sender‘s city/town. For example: Vilnius; Kauno raj.;
                'address' => $sender_warehouse_data['address'], //Sender‘s address. For example: Parokiškio km., Liepų al. 2- 53
                'post_code' => $sender_warehouse_data['p_code'], //Sender‘s post code. For examle: 23456.
                'contact_person' => $sender_warehouse_data['name'], //Contact person name to whom a courier can call
                'contact_tel' => $sender_warehouse_data['tel'], //Contact person telephone number a courier can call
            ),
            // 'consignee' => array(
            //  'name' => array(),
            //  'company_name' => array(),
            //  'country' => array(),
            //  'city' => array(), //Consignee‘s city/town. For example: Vilnius; Kauno raj.;
            //  'address' => array(), //Consignee‘s address. For example: Parokiškio km., Liepų al. 2-53
            //  'post_code' => array(), //Consignee‘s post code. For examle: 23456.
            //  'contact_person' => array(), //Contact person name to whom a courier can call
            //  'contact_tel' => array(), //Contact person telephone number a courier can call
            //  ),
            'weight' => $courier_data->getData('weight'), //Weight of pick up in kilogram
            'volume' => $courier_data->getData('volume'),
            'date_y' => date('Y',strtotime($courier_data->getData('pickup_date'))),
            'date_m' => date('m',strtotime($courier_data->getData('pickup_date'))),
            'date_d' => date('d',strtotime($courier_data->getData('pickup_date'))),
            'hour_from' => date('H',strtotime($courier_data->getData('pickup_time_from'))),
            'min_from' => date('i',strtotime($courier_data->getData('pickup_time_from'))),
            'hour_to' => date('H',strtotime($courier_data->getData('pickup_time_to'))),
            'min_to' => date('i',strtotime($courier_data->getData('pickup_time_to'))),
            'comment' => $courier_data->getData('comment'),
            'spp' => $courier_data->getData('spp'),
            'doc_no' => $courier_data->getData('doc_no'),
        );

        $xml = $this->_convertArray2Xml($OrdersInXml);
        return $xml;
    }

    public function getManifestNr($order, $warehouse_id = null)
    {
        $order_entity_id = $order->getData('entity_id');
        $warehouse_nr = Mage::getModel('venipak/orderdata')->load($order_entity_id)->getData('warehouse_id'); //grab from order config table
        if (!isset($warehouse_id)) {
            $warehouse_id = $this->getOrderValue('warehouse_id',$order); //If not set manual seek in order data
        }
        if (!isset($warehouse_id)) {
            $warehouse_id = $this->default_warehouse_id; //If not found database, set it default value
        }
        $date = explode("-",Mage::getModel('core/date')->date('y-m-d'));
        $warehouse_id = str_pad($warehouse_id, $this->warehouse_id_pad, '0', STR_PAD_LEFT);
        $manifest_nr = $this->getConfigData('id', $order->getStoreId()).$date[0].$date[1].$date[2].$warehouse_id;
        return $manifest_nr;
    }

    /**
     * Get Order data from balticode venipak order data table
     *
     * @param String $column
     * @param Object $order
     * @return Mix
     */
    public function getOrderValue($column = '', $order)
    {
        $entity_id = $order->getData('entity_id');
        $data = Mage::getModel('venipak/orderdata')->load($entity_id)->getData($column); //Get info by order
        if (!isset($data)) {
            $data = Mage::helper('venipak/data')->getConfigData($column); //if not found info by order grab default value
        }
        return $data;
    }

    /**
     * Generate Warehouse Id
     * if Warehouse is set in database return that,
     * If warehouse not set so return default value from this class warehouse_id_pad variable
     *
     * @param Object order
     * @return Warehouse Id
     */
    public function getWarehouseId($order)
    {
        $warehouse_id = $this->getOrderValue('warehouse_id', $order);
        if (empty($warehouse_id)) {
            $warehouse_id = $this->default_warehouse_id;
        }
        return $warehouse_id;
    }

    /**
     * Generate Package Numbers
     * If Pack number found in Balticode venipak order data table return that value,
     * if Pack number not found in database, generate it from Account ID and Current Pack Number in linke (increment)
     *
     * @param Order
     * @return Venipak ShipmentXML
     */
    public function getPackNr($order = null)
    {
        //$template = "V{id}E{pack_number}";
        $current_pack_number = $this->getConfigData('pack_number');
        if (empty($current_pack_number)) {
            $current_pack_number = '1';
        }
        $path = 'carriers/'.strtolower($this->_getModuleName()).'/pack_number';

        $storeId = null;
        if ($order) {
            $storeId = $order->getStoreId();
        }

        Mage::getModel('core/config')->saveConfig($path, $current_pack_number+1); //Increment packs
        Mage::app()->getStore()->resetConfig();

        $current_pack_number = str_pad($current_pack_number, 7, '0', STR_PAD_LEFT);
        $pack_nr = 'V'.$this->getConfigData('id', $storeId).'E'.$current_pack_number;
        return $pack_nr;
    }

    /**
     * Packages count
     *
     * @param   object $order
     * @return  int of packs count
     */
    public function getPackCount($order)
    {
        $pack_count = Mage::helper('venipak/data')->getOrderValue('pack_count', $order); //get from database set value
        if (empty($pack_count)) { //not found value in venipak order database
            $pack_type = $this->getConfigData('pack');
            if ($pack_type == 'item') { //Count Packs by Order or by Order items total?
                $pack_count = $order->getData('total_qty_ordered'); //Return how much ordered items
            } else {
                $pack_count = 1; //One becouse its a one Order
            }
        }
        return (int)$pack_count;
    }

    /**
     * Retrieve information from settings configuration
     *
     * @param   string $field
     * @return  mixed
     */
    public function getConfigData($field, $store_id = null)
    {
        $path = 'carriers/'.strtolower($this->_getModuleName()).'/'.$field;
        return Mage::getStoreConfig($path, $store_id);
    }

    /**
     * Array Converter to XML structure
     *
     * @param   array $array
     * @return  SimpleXMLElement Object
     */
    public function _convertArray2Xml($array)
    {
        $array2xml = Mage::getModel('venipak/array2xml');
        $xml = $array2xml->createXML('description', $array);
        return $xml->saveXML();
    }

    /**
     * Get All Warehouse List from config, This is a little bit complicate
     *
     * @return  array
     */
    public function getWarehouseList($storeId = null)
    {
        $default_warehouse = unserialize($this->getConfigData('default_warehouse', $storeId)); //Collect Default values of warehouse
        $extra_warehouse = unserialize($this->getConfigData('warehouse', $storeId)); //Collect Extra values of warehouses

        if ($default_warehouse && $extra_warehouse) { //if both is found
            $extra_warehouse = $this->flipWarehouseList($extra_warehouse);
            unset($extra_warehouse[0]);
            $extra_warehouse = $this->flipWarehouseList($extra_warehouse);
            $warehouses = array_merge_recursive($default_warehouse, $extra_warehouse); //Merge both
        } else if ($default_warehouse && !$extra_warehouse) { // If found only default value, return default value
            $warehouses = $default_warehouse; //leave only default
        } else if (!$default_warehouse && $extra_warehouse) { // If found only extra values, do some checks and clear empty values
            $warehouses = $extra_warehouse; //leave only extra
        } else if (!$default_warehouse && !$extra_warehouse) { //If both false, return false
            return false;
        }
        $this->_clearEmptyLine($warehouses,array('id')); //clear line if found empty
        $warehouses = $this->flipWarehouseList($warehouses); //Flip array
        $this->_reIndexWarehouseList($warehouses, $this->default_warehouse_id ); //Change all keys
        $warehouses = $this->flipWarehouseList($warehouses); //Flip array
        return $warehouses;
    }

    /**
     * Flip DualDimensional Array
     *
     * @param array
     * @return  array
     */
    public function flipWarehouseList($list)
    {
        $new_array = array();
        $options = array_keys($list); // get all keys c_name, name, c_post and etc
        $values_count = array_keys((array)@$list[reset($options)]); //get how much records available
//TODO fix notice
        foreach ($values_count as $row_nr => $row) { //get rows
            foreach ($options as $key => $option) {
                $new_array[$row][$option] = $list[$option][$row];
            }
        }
        return $new_array;
    }

    /**
     * Reindex Array Keys this recount array kays
     *
     * @param $warehouses - Pointer array
     *         $first_key - count by
     */
    private function _reIndexWarehouseList(&$warehouses,$first_key = 0)
    {
        foreach ($warehouses as $value) {
            $new_tmp_array[$first_key++] = $value;
        }
        $warehouses = $new_tmp_array;
    }

    /**
     * Clear Empty linke in Array
     *
     * Count all children string value, if in all parents has zero, ten unset it
     *
     * @param $warehouses - Pointer array
     *         $skip_value - skip children key
     */
    private function _clearEmptyLine(&$warehouses, $skip_values = array())
    {
        $default_warehouse_values = array_values($warehouses); //Get all warehouse info without names
        $warehouse_keys = array_keys($default_warehouse_values[0]); //Get all keys of warehouse
        $empty_warehouse_array = array_fill_keys(array_keys($warehouse_keys), null); //Leave all keys and fill array with null's, create new array with just keys
        foreach ($warehouses as $key_row => $warehouse) { //step by options -> c_name, c_company, name and etc
            if (in_array($key_row,$skip_values)) {
                continue; // If option is id skip this, becouse this value can't be empty
            }
            foreach ($warehouse as $key => $value) { //Step by warehouse
                $empty_warehouse_array[$key] += strlen(trim($value)); //Sum operation between NULL and string length Input value
            }
        }
        $empty_array_rows_keys = array_keys($empty_warehouse_array,0); //Get all Keys of rows who is empty
        foreach ($warehouses as $option => $value) {
            foreach ($empty_array_rows_keys as $key) {
                unset($value[$key]);
            }
            $all_warehouse[$option] = $value;
        }
        $warehouses = $all_warehouse;
    }

    public function getRegionsList($country_id)
    {
        return Mage::getModel('directory/region')->getResourceCollection()->addCountryFilter($country_id)->load()->toOptionArray();
    }

    public function getCountyList()
    {
        $view_id = Mage::app()->getRequest()->getParam('store');
        // if (empty($value)) {
        //     $value = Mage::getStoreConfig('general/country/default',$view_id);
        // }
        $countrys = Mage::getModel('directory/country')->getResourceCollection()->loadByStore($view_id)->toOptionArray(false);
        return $countrys;
    }

    public function getShippingPrice($country_id, $region_code = null, $weight, $subtotal = null)
    {
        $delivery_prices = unserialize($this->getConfigData('delivery_price'));
        $this->_clearEmptyLine($delivery_prices, array('country'));
        $delivery_prices = $this->flipWarehouseList($delivery_prices);

        $deliveryPriceByCountry = array();
        foreach ($delivery_prices as $delivery_price) {
            if ($delivery_price['country'] == $country_id) {
                $deliveryPriceByCountry[] = $delivery_price;
            }
        }

        $delivery_prices = $deliveryPriceByCountry;
        //All set delivery prices and weight intervals for this country
        if (count($delivery_prices)) {
            $this->_reIndexWarehouseList($delivery_prices);
            $temp = $this->flipWarehouseList($delivery_prices);
            $availible_wheight = array();
            foreach ($temp['weight'] as $key => $currentWeight) {
                if ($currentWeight >= $weight) {
                    $availible_wheight[$key] = $currentWeight;
                }
            }
            if (count($availible_wheight)) { //Test for availible
                $availible_wheight_key = array_keys($availible_wheight, min($availible_wheight));
                $this_config = $delivery_prices[reset($availible_wheight_key)];
                if ($this->getConfigData('free_enable') //free shipping is enabled
                    && (
                        (!empty($this_config['free_subtotal'])) // is not empty
                        && ($subtotal >= (float)$this_config['free_subtotal']) // cart subtotal greeter that from free shipping
                        )
                ) {
                    return 0; //Return zero price
                }
                return (float)$this_config['price'];
            }
            return false;
        }

        if ($this->getConfigData('free_enable') && ($subtotal >= (float)$this->getConfigData('free_subtotal'))) {
            return 0; //Return zero price
        }
        return $this->getConfigData('price'); //if nothing to set return fixed price
    }

    public function getDeliveryTimeLabel($time_code)
    {
        $AllDeliveryTimes = Mage::getModel('venipak/system_config_source_deliverytime')->toArray();
        if (!isset($AllDeliveryTimes[$time_code])) {
            return false;
        }
        return $AllDeliveryTimes[$time_code];
    }

    public function isCodPayment($order)
    {
        $order_payment_method = $order->getPayment()->getData('method');
        $message = 'function isCodPayment: PaymentMethod->'
            .$order_payment_method.'; Availible->'
            .print_r($this->_codPayMentMethods, true);

        Mage::getSingleton('venipak/data')->writeDebug($message);
        if (in_array($order_payment_method, $this->_codPayMentMethods)) {
            return $order->getData('base_grand_total');
        }
        return array();
    }

    public function isVenipakMethod($order)
    {
        $order_shipping_method = $order->getData('shipping_method');
        $message = 'function isVenipakMethod: ShippingMethod->'
            .$order_shipping_method.'; Availible->'
            .print_r($this->_venipakMethods, true);

        Mage::getSingleton('venipak/data')->writeDebug($message);
        return in_array($order_shipping_method, $this->_venipakMethods);
    }

    public function getWarehouseData($warehouse_id)
    {
        $warehouses = $this->flipWarehouseList($this->getWarehouseList()); //collect warehouse list
        $warehouse_data = $warehouses[$warehouse_id]; //get data about selected warehouse, adress, name and etc.
        return $warehouse_data;
    }

    public function getSystemVersion()
    {
        return Mage::getVersion();
    }

    public function getModuleVersion()
    {
        return (string) Mage::getConfig()->getNode()->modules->Balticode_Venipak->version;
    }
}
