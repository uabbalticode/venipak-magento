<?php

class Balticode_Venipak_Adminhtml_OrderController extends Mage_Adminhtml_Controller_Action
{

    /**
     * Collect post arguments
     *
     * @param Key of array
     * @return Array
     */
    private function _collectPostData($post_key = null)
    {
        return $this->getRequest()->getPost($post_key);
    }

    public function SaveAction()
    {
        $post_data = $this->_collectPostData();
        $warehouse_id_pad = Mage::helper('venipak/data')->warehouse_id_pad;
        $data = array(
            'entity_id' => $post_data['entity_id'],
            //'label_size' => $post_data['label_size'],
            'pack_count' => $post_data['pack_count'],
            'warehouse_id' => $post_data['warehouse_id'],
            'return_doc' => $post_data['return_doc'],
            'office_nr' => $post_data['office_nr'],
            'door_nr' => $post_data['door_nr'],
            'client_warehouse' => $post_data['client_warehouse'],
            'comment_call' => $post_data['comment_call'],
            'sent' => '0',
        );
        Mage::getModel('venipak/orderdata')->updateDbRow($post_data['entity_id'], $data, true);
        $this->goBack();
    }

    public function CallAction()
    {
        $this->loadLayout()->_addContent(
            $this->getLayout()->createBlock('venipak/adminhtml_sales_callvenipakcourier', 'call_venipak_courier')
        )
        ->renderLayout();
    }

    private function goBack()
    {
        $this->_redirectReferer();
    }

    protected function _isAllowed()
    {
       return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/venipak');
    }
}
