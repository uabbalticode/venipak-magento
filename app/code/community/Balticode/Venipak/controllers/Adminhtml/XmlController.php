<?php

class Balticode_Venipak_Adminhtml_XmlController extends Mage_Adminhtml_Controller_Action
{
    private $label_zip_file_name = "label";

    private $manifest_zip_file_name = "manifest";

    /**
     * Collect post arguments
     *
     * @param Key of array
     * @return Array
     */
    private function _collectPostData($post_key = null)
    {
        return $this->getRequest()->getPost($post_key);
    }

    /**
     * Fill Balticode venipak order data table
     *
     * @param selected orders ids
     * @return Array where is [manifest nr][Label type][Order entity id][] => Packs Nr
     */
    private function _fillDataBase($order_ids = array())
    {
        $pack_data = array();
        foreach ($order_ids as $order_id) {
            $pack_no = array();
            $order = Mage::getModel('sales/order')->load($order_id); //Load order
            if (!Mage::helper('venipak/data')->isVenipakMethod($order)) { //Is NOT Venipak shipping method?
                $text = 'Warning: Order '.$order->getData('increment_id').' not Venipak shipping method.';
                Mage::getSingleton('adminhtml/session')->addWarning($text);
                Mage::getSingleton('venipak/data')->writeError($text."\n".' This warning got becouse this order not shipping by Venipak system, this order is shipping is: '.$order->getData('shipping_method'));
                continue;
            }
            if (!$order->getShippingAddress()) { //Is set Shipping adress?
                $items = $order->getAllVisibleItems();
                foreach ($items as $item) {
                    $ordered_items['sku'][]=$item->getSku();
                    $ordered_items['type'][]=$item->getProductType();
                }
                $text = 'Warning: Order '.$order->getData('increment_id').' not have Shipping Adress.';
                Mage::getSingleton('adminhtml/session')->addWarning($text);
                Mage::getSingleton('venipak/data')->writeError($text."\n".' This warning got becouse in order cannot get shipping address.'."\n".'This order product types is: '.print_r($ordered_items,true));
                continue;
            }
            $venipak_data = Mage::getModel('venipak/orderdata')->load($order_id)->getData();

            if (empty($venipak_data)) { //If for this order has been not save any data to Database, so need it to Fill
                $data = array(
                        'entity_id' => $order_id,
                        'label_size' => Mage::helper('venipak/data')->getOrderValue('label_size', $order),
                        'pack_count' => Mage::helper('venipak/data')->getPackCount($order),
                        'warehouse_id' => Mage::helper('venipak/data')->default_warehouse_id,
                        'return_doc' => Mage::helper('venipak/data')->getOrderValue('return_doc', $order),
                        'office_nr' => "",
                        'door_nr' => "",
                        'comment_call' => Mage::helper('venipak/data')->getOrderValue('comment_call', $order),
                        'sent' => '0',
                    );
                Mage::getModel('venipak/orderdata')->updateDbRow($order_id, $data, true);
                $venipak_data = $data; //Set data renew so when we can use it.
            }

            if (!$venipak_data['sent']) { //This Order is New so create XML and send to Venipak API to Register
                $venipak_data['manifest_nr'] = Mage::helper('venipak/data')->getManifestNr($order);
                $xmlData = Mage::helper("venipak/data")->getShipmentXmlData($order);
                $api_response = Mage::helper('venipak/api')->setStore($order->getStoreId())->sendData(null,$xmlData);

                if ($api_response) {
                    $api_response_xml = new SimpleXMLElement($api_response);
                    $api_response_array = Mage::getModel('venipak/data')->objectToArray(json_decode(json_encode((array)$api_response_xml), TRUE));
                    if ($api_response_array['@attributes']['type'] == 'error') { //If responce is wrong, register error
                        foreach ($api_response_xml->error as $value) {
                            $error_text = Mage::getModel('venipak/data')->objectToArray(json_decode(json_encode((array)$value), TRUE));
                            $text = 'Error code:'.$error_text['@attributes']['code'].' Error Message:'.$error_text['text'].' Order nr:'.$order_id.' Order Increment nr:'.$this->_getOrderData($order_id,'increment_id');
                            Mage::getSingleton('adminhtml/session')->addError($text);
                            Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got from Venipak API. Full error report: '.print_r($api_response,true)."\nTo API send XML data:".print_r($xmlData,true));
                        }
                        continue;
                    } elseif ($api_response_array['@attributes']['type'] == 'ok') { //If this Order has been success to send to API
                        $responce = get_object_vars($api_response_xml);
                        if (is_array($api_response_array['text'])) {
                            foreach ($api_response_array['text'] as $value) { //Collect all Package numbers
                                $pack_no[] = $value;
                            }
                        } else {
                            $pack_no[] = $api_response_array['text'];
                        }
                        $data = array(
                            'manifest_nr' => $venipak_data['manifest_nr'],
                            "sent" => "1",
                            "pack_no" => serialize($pack_no),
                        );
                        Mage::getModel('venipak/orderdata')->updateDbRow($order_id, $data);//Set in database to sent
                    }
                } else {
                    $this->_redirectReferer();
                    return false;
                }
            } else {
                $pack_no = unserialize(Mage::helper('venipak/data')->getOrderValue('pack_no', $order));
            }
            //Now we have all request data for this order, so all them o array
            $pack_data[$venipak_data['manifest_nr']][$venipak_data['label_size']][$order_id] = $pack_no;
        }

        return $pack_data;
    }

    /**
     * Make downloaded file from file name list, if more that one file zip files
     *
     * @param file name list
     * @param Ziped file name
     * @return Array where is [manifest nr][Label type][Order entity id][] => Packs Nr
     */
    private function _compressAndDownload($success_files, $zip_file_name = "archive")
    {
        $temp_directory = Mage::getBaseDir('tmp').'/';
        if (count($success_files) === 1) { //If this just one file just make it download
            $file_content = file_get_contents($temp_directory.$success_files[0]);
            $this->_prepareDownloadResponse($success_files[0], $file_content, "application/pdf");
            unlink($temp_directory.$success_files[0]); //Delete temp file
        } elseif (count($success_files) > 1) { //If more that one then zip all files and send like one
            $zip_name = $zip_file_name."-".Mage::getModel('core/date')->date('y-m-d').".zip";
            $zip = new ZipArchive();
            $zip->open($temp_directory.$zip_name, ZipArchive::OVERWRITE);
            foreach ($success_files as $file) {
                $zip->addFile($temp_directory.$file, $file);
            }
            $zip->close();
            foreach ($success_files as $file) {
                unlink($temp_directory.$file); //Delete zip file content
            }
            $file_content = file_get_contents($temp_directory.$zip_name);
            $this->_prepareDownloadResponse($zip_name, $file_content);
            unlink($temp_directory.$zip_name); //delete temp zip file
        } else {
            $this->_redirectReferer();
        }
    }

    /**
     * Get Order data by order Id
     *
     * @param Order entity id
     * @param Order data who return
     * @return value from Sales/Order object
     */
    private function _getOrderData($order_id, $order_data)
    {
        $order = Mage::getSingleton('sales/order')->load($order_id); //Load order
        return $order->getData($order_data);
    }

    /**
     * Generate ShipmentXML
     *
     * Test Data if all correct, @return Venipak Lables
     */
    public function ShipmentXmlAction()
    {
        $pack_data = array();
        $success_files = array();
        $order_ids = $this->_collectPostData('order_ids');
        if (!isset($order_ids)) {
            return false; //If something wrong
        }
        $pack_data = $this->_fillDataBase($order_ids); //Send data to server and get packs number's
        //Now we have all selected orders info of packs
        if (!count($pack_data) || $pack_data === false) { //If nothing to print
            $this->_redirectReferer();
        } else { //If found Order who can get Label so Do it
            $temp_directory = Mage::getBaseDir('tmp').'/';
            $label_type = Mage::helper('venipak/data')->getConfigData('label_size');

            foreach ($pack_data as $manifest_nr => $orders) { //Foreach Manifest number
                $manifest_array[]= $manifest_nr;
                foreach ($orders as $label_type => $order) { //Foreach different label types
                    foreach ($order as $packs) { //Gel all pack numbers of thise orders
                        foreach ($packs as $pack) {
                            $packs_array[] = $pack;
                        }
                    }
                }
            }

            $label = Mage::helper('venipak/api')->getPackLabelPdfFile($packs_array, null, $label_type);
            if (!$label) {
                $this->_redirectReferer();
            }
            if (strpos(strtok($label, "\n"),'%PDF-') === false) { //test of response, if it not a file, register like error
                $text = 'Error: Venipak API message: '.$label;
                Mage::getSingleton('adminhtml/session')->addError($text);
                Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got when in balticode_venipak_order_data found registed data about order, but in Venipak API these info has been not found.'."\n".'Date who has been send:'."\n".'$pack_nrs:'."\t".(print_r($packs_array,true)).' $manifest_nr:'."\t".print_r($manifest_array,true).' $label_type:'."\t".$label_type);
                $this->_redirectReferer();
            }
            $file_name = 'Labels-'.date('y-m-d').".pdf";
            $bytes = file_put_contents($temp_directory.$file_name, $label);
            if ($bytes) {
                $success_files[] = $file_name;
            } else {
                $text = 'Warning: From Venipak API not get file content.';
                Mage::getSingleton('adminhtml/session')->addWarning($text);
                Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got when file content LENGHT who has been put to server is zero.'."\n".'Using data:'."\n".'$temp_directory.$file_name:'."\t".$temp_directory.$file_name.' $label:'."\t".substr($label, 0, 100).'...'."\n".'Date who has been send:'."\n".'$pack_nrs:'."\t".(print_r($pack_nrs,true)).' $manifest_nr:'."\t".print_r($manifest_array,true).' $label_type:'."\t".$label_type);
            }

            /*
            foreach ($pack_data as $manifest_nr => $orders) { //Foreach Manifest number
                foreach ($orders as $label_type => $order) { //Foreach different label types
                    $pack_nrs = array();
                    foreach ( $order as $packs ){ //Gel all pack numbers of thise orders
                        $pack_nrs = array_merge_recursive($pack_nrs,$packs);
                    }

                    $label = Mage::helper('venipak/api')->getPackLabelPdfFile($pack_nrs, $manifest_nr, $label_type);
                    if(!$label) $this->_redirectReferer();
                    if(strpos(strtok($label, "\n"),'%PDF-') === FALSE){ //test of response, if it not a file, register like error
                        $orders_id = array_keys($order); //get all Orders Id's
                        $order_increment_ids = '';
                        foreach ($orders_id as $order_id) {
                            $order_increment_ids .= ' #'.$this->_getOrderData($order_id,'increment_id');
                        }
                        $text = 'Error: Manifest nr.: '.$manifest_nr.' Orders:'.$order_increment_ids.' API message: '.$label;
                        Mage::getSingleton('adminhtml/session')->addError($text);
                        Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got when in balticode_venipak_order_data found registed data about order, but in Venipak API these info has been not found.'."\n".'Date who has been send:'."\n".'$pack_nrs:'."\t".(print_r($pack_nrs,true)).' $manifest_nr:'."\t".$manifest_nr.' $label_type:'."\t".$label_type);
                        continue;
                    }

                    $labels = Mage::getModel('venipak/system_config_source_labeltype')->toArray(); //Get title of label
                    $file_name = $manifest_nr."-".$label_type.".pdf";
                    $bytes = file_put_contents($temp_directory.$file_name, $label);
                    if($bytes){
                        $success_files[] = $file_name;
                    } else {
                        $text = 'Warning: Order '.$manifest_nr.' From Venipak API not get file content.';
                        Mage::getSingleton('adminhtml/session')->addWarning($text);
                        Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got when file content LENGHT who has been put to server is zero.'."\n".'Using data:'."\n".'$temp_directory.$file_name:'."\t".$temp_directory.$file_name.' $label:'."\t".substr($label, 0, 100).'...'."\n".'Date who has been send:'."\n".'$pack_nrs:'."\t".(print_r($pack_nrs,true)).' $manifest_nr:'."\t".$manifest_nr.' $label_type:'."\t".$label_type);
                    }
                }
            }
            */
            //$success_files = array_unique($success_files);
            //Now we have all downloadet pdf files to var directory
            $this->_compressAndDownload($success_files,$this->label_zip_file_name);
        }
    }

    /**
     * Generate OrderXml
     *
     * Test Data if all correct, @return Venipak Manifest data
     */
    public function OrderXmlAction()
    {
        $success_files = array();
        $order_ids = $this->_collectPostData('order_ids');
        if (!isset($order_ids)) {
            return false; //If something wrong
        }
        $pack_data = $this->_fillDataBase($order_ids); //Send data to server and get packs number's
        $temp_directory = Mage::getBaseDir('tmp').'/';
        if (!count($pack_data) || $pack_data === false) {
            $this->_redirectReferer();
            return;
        }
        $manifest_nrs = array_keys($pack_data);
        if (1) {
            $manifest = Mage::helper('venipak/api')->getSenderShipmentBillPdfFile($manifest_nrs);
            if (strpos(strtok($manifest, "\n"),'%PDF-') === FALSE) { //test of response, if it not a file, register like error
                $order_increment_ids = '';
                foreach ($pack_data as $manifest_nr => $label_order) {
                    foreach ($label_order as $label_type => $orders) {
                        foreach ($orders as $order_id => $pack_nrs) {
                            $order_increment_ids .= ' #'.$this->_getOrderData($order_id,'increment_id');
                        }
                    }
                }
                $text = 'Error: Manifest nr.: '.print_r($manifest_nrs,true).' Orders:'.$order_increment_ids.' API message: '.$manifest;
                Mage::getSingleton('adminhtml/session')->addError($text);
                Mage::getSingleton('venipak/data')->writeError($text."\n".'Send info: $manifest_nr:'."\t".print_r($manifest_nrs,true));
                $this->_redirectReferer();
                return;
            } else {
                $file_name = $this->manifest_zip_file_name.".pdf";
                $bytes = file_put_contents($temp_directory.$file_name, $manifest);
                if ($bytes) {
                    $success_files[] = $file_name;
                } else {
                    unlink($temp_directory.$file_name); // Delete file with zero content
                    $text = 'Warning: Manifest '.$this->manifest_zip_file_name.' From Venipak API not get file content.';
                    Mage::getSingleton('adminhtml/session')->addWarning($text);
                    Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got when file content LENGHT who has been put to server is zero.'."\n".'Using data:'."\n".'$shippment_bill_number:'.$shippment_bill_number."\n".'Response: $manifest: '.$manifest);
                    $this->_redirectReferer();
                    return;
                }
            }
        } else {
            foreach ($manifest_nrs as $shippment_bill_number) {
                $manifest = Mage::helper('venipak/api')->getSenderShipmentBillPdfFile($shippment_bill_number);
                if (strpos(strtok($manifest, "\n"),'%PDF-') === FALSE) { //test of response, if it not a file, register like error
                    $order_increment_ids = '';
                    foreach ($pack_data[$shippment_bill_number] as $label_type => $orders) {
                        foreach ($orders as $order_id => $pack_nrs) {
                            $order_increment_ids .= ' #'.$this->_getOrderData($order_id,'increment_id');
                        }
                    }
                    $text = 'Error: Manifest nr.: '.$shippment_bill_number.' Orders:'.$order_increment_ids.' API message: '.$manifest;
                    Mage::getSingleton('adminhtml/session')->addError($text);
                    Mage::getSingleton('venipak/data')->writeError($text."\n".'Send info: $manifest_nr:'."\t".print_r($shippment_bill_number,true));
                    continue;
                }

                $file_name = $shippment_bill_number.".pdf";
                $bytes = file_put_contents($temp_directory.$file_name, $manifest);
                if ($bytes) {
                    $success_files[] = $file_name;
                } else {
                    unlink($temp_directory.$file_name); // Delete file with zero content
                    $text = 'Warning: Manifest '.$shippment_bill_number.' From Venipak API not get file content.';
                    Mage::getSingleton('adminhtml/session')->addWarning($text);
                    Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got when file content LENGHT who has been put to server is zero.'."\n".'Using data:'."\n".'$shippment_bill_number:'.$shippment_bill_number."\n".'Response: $manifest: '.$manifest);
                    continue;
                }
            }
        }
        //$success_files = array_unique($success_files);
        //Now we have all downloadet pdf files to var directory
        $this->_compressAndDownload($success_files, $this->manifest_zip_file_name);
    }

    public function CallcourierAction()
    {
        $post_data = $this->_collectPostData(); //Collect Post data from form
        $selected_orders = explode(',', $post_data['selected_orders']);

        Mage::getModel('venipak/courierdata')->addDbRow($post_data); // Add new data in balticode_venipak_courier_data

        $id = Mage::getModel('venipak/courierdata')->getDbLastId(); // Get last inserted ID
        $xmlData = Mage::helper('venipak/data')->getCourierXmlData($id, $selected_orders); // Get XML Data by this ID
        $api_response = Mage::helper('venipak/api')->sendData(null, $xmlData); //Send XML data su VeniPak API
        if ($api_response) {
            $api_response_xml = new SimpleXMLElement($api_response); //Response convert to XML Object from string
            $api_response_array = Mage::getModel('venipak/data')->objectToArray(json_decode(json_encode((array)$api_response_xml), TRUE)); //XML Object convert to array
            if ($api_response_array['@attributes']['type'] == 'error') { //If responce status is wrong, register error
                foreach ($api_response_xml->error as $value) {
                    $error_text = Mage::getModel('venipak/data')->objectToArray(json_decode(json_encode((array)$value), TRUE));
                    $text = 'Error code:'.$error_text['@attributes']['code'].' Error Message:'.$error_text['text'];
                    Mage::getSingleton('adminhtml/session')->addError($text);
                    Mage::getSingleton('venipak/data')->writeError($text."\n".'This error got from Venipak API. Full error report: '.print_r($api_response,true)."\nTo API send XML data:".print_r($xmlData,true));
                    Mage::getModel('venipak/courierdata')->removeDbRow($id);
                }
            } elseif ($api_response_array['@attributes']['type'] == 'ok') { //If this Order has been success to send in API Grab code and put in database
                Mage::getModel('venipak/courierdata')->updateDbRow($id,array('code' => $api_response_array['text'])); //Update courier data
                Mage::getSingleton('adminhtml/session')->addSuccess('Courier call success ID:'.$api_response_array['text']);
            }
        } else {
            Mage::getModel('venipak/courierdata')->removeDbRow($id);
        }
        $this->_redirectReferer();
    }

    protected function _isAllowed()
    {
       return Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/venipak');
    }
}
