var fromH = 0; //hour
var fromM = 0 //min
var toH = 0; //hour
var toM = 0; //min

function callVenipakCourier(url, title) {
	if ($('browser_window') && typeof(Windows) != 'undefined') {
        Windows.focus('browser_window');
        return;
    }
    var orders = collectCheckBoxValues('order_ids');
	var dialogWindow = Dialog.info(
        null, {
            closable:true,
            resizable:true,
            draggable:true,
            className:'magento',
            windowClassName:'popup-window',
            title: title,
            top:50,
            width:490,
            height:400,
            zIndex:1000,
            recenterAuto:false,
            hideEffect:Element.hide,
            showEffect:Element.show,
            id:'browser_window',
            url:url + '?order_ids=' + orders,
            onClose:function (param, el) {
            }
        }
    );
};

function getValue(id)
{
    var e = document.getElementById(id);
    var value = e.options[e.selectedIndex].value;
    return value;
}

function collectTime()
{
    addFromHours(getValue('time_from_h'));
    addFromMin(getValue('time_from_m'));
    addToHours(getValue('time_to_h'));
    addToMin(getValue('time_to_m'));
}

function addFromHours(hours)
{
    fromH = parseInt(hours);
    setdifference();
}
function addFromMin(min)
{
    fromM = parseInt(min);
    setdifference();
}
function addToHours(hours)
{
    toH = parseInt(hours);
    setdifference();
}
function addToMin(min)
{
    toM = parseInt(min);
    setdifference();
}
function setdifference()
{
    difference_time = (((toH*60)+toM) - ((fromH*60)+fromM));
    if(difference_time > 0){
        document.getElementById('time_difference').value = difference_time;
    }
}
function collectCheckBoxValues($elementName)
{
    var x = document.getElementsByName($elementName);
    var o = [];
    var i;
    for (i = 0; i < x.length; i++) {
        if (x[i].type == "checkbox" && x[i].checked == true) {
            o.push(x[i].value);
        }
    }

    return o;
}
function validateOrdersId($elementName)
{
    var o = collectCheckBoxValues($elementName);
    if (o.length == 0) {
        alert(Translator.translate('Please select Order'));
        return false;
    }

    return true;
}